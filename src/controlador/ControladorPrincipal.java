package controlador;

import java.io.IOException;
import java.util.HashSet;
import java.util.Iterator;
import modelo.*;
import vista.*;

/**
 *Fichero: Controlador.java
 * @author Camelia Gheorghiu <camelianaa@hotmail.com>
 * @date 02-dic-2015
 */
public class ControladorPrincipal {
        private IModelo m = null;
        private VistaPrincipal v = new VistaPrincipal();
        
        private IVista<Hotel> vistaHotel;
        private IVista<Turista> vistaTurista = new VistaTurista();
        private IVista<Administrador> vistaAdmin = new VistaAdministrador();
        private static VistaTurista vT = new VistaTurista();
        private Turista turista;
        private Administrador admin;
        FuncionesTerminal terminal = new FuncionesTerminal();
        
    ControladorPrincipal(IModelo modelo, VistaPrincipal vista) throws IOException{
        this.m = modelo;
        v = vista;
       
        while(m==null) {
            
            m = this.procesarMenuEstructura();
            break;
	}
        this.procesarMenuAcceso();
       // this.procesarMenu();
        
            
    }

    
    //MENÚ ESTRUCTURA
    private IModelo procesarMenuEstructura() throws IOException{
    
    boolean opcioninvalida = false;
    char menu;
		do {
                    menu = v.menuEstructura();
			switch (String.valueOf(menu)){
                           // switch (menu){
				case "s" :
				//case "E" :
                                    terminal.salir();
                                    break;
				case "a" :
				//case "A" :
                                    m = new ModeloArray();
                                    break;
				case "h":
				//case "H":
                                    m = new ModeloHashset();
                                    break;
                                
                                default:
                    //mostrar error si se introduce un número u otra letra para entrar al menú
                   FuncionesTerminal.mostrarError("Error:  Debe ser una de las siguientes letras:s, a, h.");
			}
			opcioninvalida = true;
		} while (m == null);
                
            return m;
    }
    
        //MENÚ ACCESO
    private void procesarMenuAcceso() throws IOException{
            
        char menu_vista;
        
            do{
        
                menu_vista = v.menuAcceso();
            
                //recorremos el menú
                switch (menu_vista){
            
                    case 's':
                        //salimos del menú principal
                        procesarMenuEstructura();
                        break;
                    case 'r':
                        create();
                        break;
                    case 'l':
                        if (turista==null){
                            inicializaObjetos();
                            m.create(turista);
        }
                        pedirUsuario();
                        pedirPass();
                        procesarMenu();
                        break;
                default:
                    //mostrar error si se introduce un número u otra letra para entrar al menú
                    FuncionesTerminal.mostrarError("Error:  Debe ser una de las siguientes letras:s, r, l.");      
            }
        }while (menu_vista != 's');
           
    }
    
    //MENÚ MODELO
    public void procesarMenu() throws IOException{
            
        char menu_vista;
        
       /* if (turista==null){
            inicializaObjetos();
        }*/
        
            do{
        
                menu_vista = v.menuModelo();
            
                //recorremos el menú
                switch (menu_vista){
            
                    case 's':
                        //salimos del menú principal
                        procesarMenuAcceso();
                        break;
                    case 't':
                        vistaTurista = new VistaTurista();
                        ControladorTurista ct = new ControladorTurista(m, vistaTurista);
                        break;
                    case 'h':
                        pedirEntrarHotel();
                        vistaHotel = new VistaHotel();
                        ControladorHotel ch = new ControladorHotel(m,vistaHotel);
                        break;
                    case 'r':
                        ControladorReserva cr= new ControladorReserva(m);
                    
                    break;
                default:
                    //mostrar error si se introduce un número u otra letra para entrar al menú
                    v.error(1);      
            }
        }while (menu_vista != 's');
           
    }

    private void inicializaObjetos() {

        Turista turista = new Turista();
        Reserva reserva = new Reserva();
        Hotel hotel = new Hotel();
        Administrador admin = new Administrador();
        
        //objeto turista
        turista.setId("t0");
        turista.setApellido("Pérez");
        turista.setNombre("Antonio");
        turista.setDni("29874287D");
        turista.setRole(1);//1 es admin
        turista.setUsuario("123");//usuario
        turista.setPassword("123");//pass
        
        admin.setDepartamento("administración");
        
        //objeto hotel
        hotel.setIdHotel("h0");
        hotel.setNombreHotel("Astoria");
        hotel.setPaisHotel("España");
        
        //objeto reserva
        reserva.setIdReserva("r0");
        reserva.setFecha("12.12.2015");
        reserva.setPrecio(200);
        
        
        reserva.setHotel(hotel);
        reserva.setTurista(turista);
        
       // m.create(turista);
        m.create(admin);
        m.create(hotel);
        m.create(reserva);

    }
     
    private void create() {
        
        turista = new Turista();
        Administrador administrador = new Administrador();
       // inicializaObjetos(); 
        turista = vistaTurista.obtener();
        administrador = vistaAdmin.obtener();
         inicializaObjetos(); 
        m.create(turista);
        m.create(administrador);
        
    }   
    
    
    private Turista pedirUsuario() {
        
        Turista turista = null;
        String user = v.mensajeUsuario();
        
        
        // recoremos la lista de turistas para sacar el usuario buscado
        HashSet turistas = m.readTurista();
        Iterator iter = turistas.iterator();
        
        while (iter.hasNext()) {
            Turista tur = (Turista) iter.next();
            if (user.equals(tur.getUsuario())) {
                turista = tur;
            }else{
                v.error(9); 
                return pedirUsuario();
            }
        } 
        return turista;
    }
    
    private Turista pedirPass() {
        Turista turista = null;
        String mensaje = v.mensajePass();//mensaje para introducir pass turista
        
        // recoremos la lista de turistas para sacar el pass buscado
        HashSet turistas = m.readTurista();
        Iterator iter = turistas.iterator();
        
        while (iter.hasNext()) {
            Turista tur = (Turista) iter.next();
            if (mensaje.equals(tur.getPassword())) {
                turista = tur;
            }else{
                v.error(10); 
                return pedirPass();
            }
        } 
        return turista;
    }
    
    
    public Turista pedirEntrarHotel() throws IOException {
        Turista turista = null;
        
        String idt = v.mensajeIDTurista();
        
        // recoremos la lista de turistas para sacar el id y role y comprobamos si es admin
        HashSet turistas = m.readTurista();
        Iterator iter = turistas.iterator();
        
        while (iter.hasNext()) {
            Turista tur = (Turista) iter.next();
            if (idt.equals(tur.getId())&& (tur.getRole()==1)) {
                turista = tur;
            }else{
                v.error(11);
                procesarMenu();
            }
        } 
        return turista;
    }
    
    
    


}

