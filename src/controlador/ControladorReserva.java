package controlador;

import java.io.IOException;
import java.util.HashSet;
import java.util.Iterator;
import modelo.*;
import vista.*;

/**
 *Fichero: ControladorReserva.java
 * @author Camelia Gheorghiu <camelianaa@hotmail.com>
 * @date 06-dic-2015
 */
public class ControladorReserva {
    
    private static IModelo modelo = null;
    private static IVista <Reserva> vistar = new VistaReserva();
    private static VistaReserva vr = new VistaReserva();
    private static FuncionesTerminal terminal = new FuncionesTerminal();
    private static VistaPrincipal v = new VistaPrincipal();
    private static Turista turista;
    private static Hotel hotel;
    private static Reserva reserva = new Reserva();
    private static String idt, idh, idr;
    
    
    public ControladorReserva(IModelo m) throws IOException{
         
        modelo = m;
	char opcion= ' ';
        
	do {
            opcion = v.menuCrud();
		switch (opcion){
                    case'v': //Volver
                        terminal.volver();
                        break;
                    case 'c': //Crear/Añadir
                        create();
                        break;
                    case 'r': // Read / Obtener / Listar
                        read();
                        break;
                    case 'u': // Actualizar
                        update();
                        break;
                    case 'd': // Borrar
                        delete();
                        break;
                    default:
                    //mostrar error si se introduce un número u otra letra para entrar al menú
                        v.error(5);
		}
        
        } while(opcion != 'v');
    }
    
    //FUNCIONES CRUD
    
    private static void create() {
        
        Reserva reserva = vistar.obtener();
        Turista turista = pedirIdTurista();
        Hotel hotel = pedirIdHotel();
        // con un if-else obtenemos el turista y reserva si existen
        if (turista!=null && hotel!=null) {
            reserva.setTurista(turista);
            reserva.setHotel(hotel);
            modelo.create(reserva);
        } else {
            v.error(6);
        }
         
    }
    
    private static void read() {
        
        //si hay reservas creadas se muestran si no, mensaje de error
        if (reserva!=null) {
            HashSet hashset = new HashSet();
            hashset = modelo.readReserva();
            vr.mostrarReservas(hashset);
        } else {
            v.error(7);
        }
    }

    private static void update() {

        idr = vr.actualizar();
        reserva = vistar.obtener();
        reserva.setIdReserva(idr);
        modelo.update(reserva);
    }

    private static void delete() {

        idr = vr.borrar();
        reserva = new Reserva(idr, "", 0.0, null, null);
        modelo.delete(reserva);
    }
    
    //FUNCIONES PARA RECORRER LAS LISTAS DEL TURISTA Y HOTEL PARA SACAR EL ID
    private static Turista pedirIdTurista() {
        Turista turista = null;
        String idt = v.mensajeIDTurista();//mensaje para introducir ID turista
        
        // recoremos la lista de turistas para sacar el id buscado
        HashSet turistas = modelo.readTurista();
        Iterator iter = turistas.iterator();
        
        while (iter.hasNext()) {
            Turista tur = (Turista) iter.next();
            if (idt.equals(tur.getId())) {
                turista = tur;
            }
        } 
        return turista;
    }
    
    private static Hotel pedirIdHotel() {
        Hotel hotel = null;
        String idh = v.mensajeIDHotel();//mensaje para introducir ID hotel
        
        // recoremos la lista de hoteles para sacar el id buscado
        HashSet hoteles = modelo.readHotel();
        Iterator iter = hoteles.iterator();
        
        while (iter.hasNext()) {
            Hotel hot = (Hotel) iter.next();
            if (idh.equals(hot.getIdHotel())) {
                hotel = hot;
            }
        } 
        return hotel;
    }
}

