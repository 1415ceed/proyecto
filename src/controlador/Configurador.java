package controlador;

import vista.FuncionesTerminal;
import vista.VistaPrincipal;

/**
 *Fichero: Configurador.java
 * @author Camelia Gheorghiu <camelianaa@hotmail.com>
 * @date 02-dic-2015
 */
 public class Configurador {
    private static String titulo;
    private static Configurador instancia;
    private static VistaPrincipal v = new VistaPrincipal();
    
    //constructor private para crear sólo una instancia del objeto
    private Configurador (String titulo){
        this.titulo = titulo;
    }
    
    //método que retorna una instancia única del objeto
    public static Configurador getInstancia (String titulo){
        
        //creamos una instancia sólo si no hay otra creada si es null
        if (instancia == null){
            instancia = new Configurador(titulo);
            Configurador.titulo = titulo;
            FuncionesTerminal.mostrarTexto("Entrando aplicación .....");
        }else{
            FuncionesTerminal.mostrarError("Error: Solo se permite una instancia");
        }
        
        return instancia;
        
    }
    
    public static String getTitulo(){
        return titulo;
    }
}