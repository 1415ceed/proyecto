package controlador;

import java.io.IOException;
import java.util.HashSet;
import java.util.Iterator;
import modelo.*;
import vista.FuncionesTerminal;
import vista.*;

/**
 *Fichero: ControladorHotel.java
 * @author Camelia Gheorghiu <camelianaa@hotmail.com>
 * @date 06-dic-2015
 */
public class ControladorHotel {
    private static IModelo modelo;
    private static IVista <Hotel> vistah = new VistaHotel();
    private static VistaHotel vH = new VistaHotel();
    private static VistaPrincipal v = new VistaPrincipal();
    private static FuncionesTerminal terminal = new FuncionesTerminal();
    private static Hotel hot = new Hotel();
    private static String id, idt;
    
    
    public ControladorHotel(IModelo m, IVista <Hotel> vistah) throws IOException{
    
        modelo = m;
	char opcion= ' ';
        
	do {
            opcion = v.menuCrud();
		switch (opcion){
                    case'v': //Volver
                        terminal.volver();
                        break;
                    case 'c': //Crear/Añadir
                        create();
                        break;
                    case 'r': // Read / Obtener / Listar
                        read();
                        break;
                    case 'u': // Actualizar
                        update();
                        break;
                    case 'd': // Borrar
                        delete();
                        break;
                    default:
                    //mostrar error si se introduce un número u otra letra para entrar al menú
                        v.error(5);
		}
        } while(opcion != 'v');
    }
    
    //FUNCIONES CRUD
    
    private static void create() {
        
        hot = vistah.obtener();
        modelo.create(hot);
    }
    
    public static void read() {
        if (hot!=null) {
            HashSet hs = new HashSet();
            hs = modelo.readHotel();
            vH.mostrarHoteles(hs);
        } else {
            v.error(8);
        }
    }

    private static void update() {

        id = vH.actualizar();
        hot = vistah.obtener();
        hot.setIdHotel(id);
        modelo.update(hot);
    }

    private static void delete() {

        id = vH.borrar();
        hot = new Hotel(id, "","");
        modelo.delete(hot);
    }
}
