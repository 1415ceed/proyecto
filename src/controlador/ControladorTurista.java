package controlador;

import java.io.IOException;
import java.util.HashSet;
import modelo.*;
import vista.*;

/**
 *Fichero: ControladorTurista.java
 * @author Camelia Gheorghiu <camelianaa@hotmail.com>
 * @date 06-dic-2015
 */
public class ControladorTurista {
    private static IModelo modelo;
    private static IVista <Turista> vistat = new VistaTurista();
    private static VistaTurista vT = new VistaTurista();
    private static VistaPrincipal v = new VistaPrincipal();
    private static FuncionesTerminal terminal = new FuncionesTerminal();
    private static  Turista tur = new Turista();
    private static String id;
    
    public ControladorTurista(IModelo m, IVista <Turista> vistat) throws IOException{
        
        modelo = m;           
	char opcion= ' ';
	
	do {
            opcion = vT.menuCrud();
		switch (opcion){
                    case'v': //Volver
                        terminal.volver();
                        break;
                   /* case 'c': // Crear/Añadir/ Se crea en el menu registrarse
                        create();
                        break;*/
                    case 'r': // Read / Obtener / Listar
                        read();
                        break;
                    case 'u':  // Actualizar
                        update();
                        break;
                    case 'd': // Borrar
                        delete();
                        break;
                    default:
                    //mostrar error si se introduce un número u otra letra para entrar al menú
                        FuncionesTerminal.mostrarError("Error:  Debe ser una de las siguientes letras: v, r, u,d.");
                    }
        
        } while(opcion != 'v');
    }
    
    //FUNCIONES CRUD
    
  /*  private static void create() {
        
        tur = vistat.obtener();
        modelo.create(tur);
    }*/
    
    private static void read() {

        HashSet hs = new HashSet();
        hs = modelo.readTurista();
        vT.mostrarTuristas(hs);
    }

    private  static void update() {

        id = vT.actualizar();
        tur = vistat.obtener();
        tur.setId(id);
        modelo.update(tur);
    }

    private static void delete() {

        id = vT.borrar();
        tur = new Turista(id, "","",0,"","","");
        modelo.delete(tur);
    }
}