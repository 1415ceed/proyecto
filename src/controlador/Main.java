/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package controlador;

import java.io.IOException;
import modelo.*;
import vista.*;

/**
 * Fichero: Main.java
 * @author Camelia Gheorghiu <kami.cg78@gmail.com>
 * @date Nov 13, 2015
 */
public class Main {

    public static void main(String[] args) throws IOException {
        
        IModelo modelo= null;
        VistaPrincipal vista = new VistaPrincipal();
        ControladorPrincipal controlador = new ControladorPrincipal(modelo, vista);
        
    }
}
        
