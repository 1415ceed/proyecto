package vista;

/**
 * Fichero: IVista.java
 * @author Camelia Gheorghiu <kami.cg78@gmail.com>
 * @date Nov 18, 2015
 */
public interface IVista<T> {
    
  //  public char menuCrud();
    public T obtener();

    public void mostrar(T t);
    
}

