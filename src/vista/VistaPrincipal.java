package vista;

import controlador.Configurador;
import java.io.IOException;

/**
 * Fichero: VistaPrincipal.java
 * @author Camelia Gheorghiu <camelianaa@hotmail.com>
 * @date 06-dic-2015
 */

public class VistaPrincipal {
    FuncionesTerminal fun = new FuncionesTerminal();
       
    //creamos el menú estructura
    public char menuEstructura() throws IOException{
        char letra;
        Configurador c = Configurador.getInstancia("AGENCIA VIAJE");
        
         System.out.println("");
        System.out.println("================");
        System.out.println("| " + Configurador.getTitulo() + " |");
        System.out.println("================");
        System.out.println("\nMENÚ ESTRUCTURA");
        System.out.println("-----------------");
        System.out.println("s".charAt(0)+ ". Salir");
        System.out.println("-----------------");
        System.out.println("a".charAt(0)+". Array");
        System.out.println("h".charAt(0)+". Hashset");
        System.out.print("Elegir opción: ");
        letra = FuncionesTerminal.PedirCaracter();
 
        return letra;
}
    //creamos el menú acceso
    public char menuAcceso() throws IOException{
        
        char menu; 
        System.out.println();
        System.out.println("MENÚ ACCESO");
        System.out.println("-----------------");
        System.out.println("s".charAt(0)+ ". Salir");
        System.out.println("-----------------");
        System.out.println("r".charAt(0)+ ". Registrarse (Nuevo)");
        System.out.println("l".charAt(0)+ ". Loggin (Existe)");
        System.out.print("Elegir opción: ");
        menu = FuncionesTerminal.PedirCaracter();
        
        return menu;

    }
    
    //creamos el menú principal
    public char menuModelo() throws IOException{
        
        char menu; 
        System.out.println();
        System.out.println("MENÚ MODELO");
        System.out.println("-----------------");
        System.out.println("s".charAt(0)+ ". Salir");
        System.out.println("-----------------");
        System.out.println("t".charAt(0)+ ". Turista");
        System.out.println("h".charAt(0)+ ". Hotel");
        System.out.println("r".charAt(0)+ ". Reserva");
        System.out.print("Elegir opción: ");
        menu = FuncionesTerminal.PedirCaracter();
        
        return menu;

    }
    //MENÚ CRUD
    public char menuCrud() throws IOException{
	
                char letra;	
                System.out.println("\n MENÚ CRUD");
                System.out.println("=====================");
                System.out.println("v".charAt(0)+ ". Volver");
                System.out.println("-------------------------");
		System.out.println("c".charAt(0)+ ". Create");
		System.out.println("r".charAt(0)+ ". Read");
		System.out.println("u".charAt(0)+ ". Update");
		System.out.println("d".charAt(0)+ ". Delete");
                System.out.print("Opción:  ");
                letra = FuncionesTerminal.PedirCaracter();
    
		return letra;

	}
    
    //comprobamos errores de tipo int para el precio de la resereva y de tipo string para la opción del menú
    
    public void error(int i){
        
        Errores error = new Errores();
        String s = error.opcion(i);
        System.out.println("\nERROR!! " +s);
    
    }
    
    //función para pedir el id del turista
    public String mensajeIDTurista() {
        System.out.println("Introduce tu id de turista: ");
        String id = fun.pedirString();
        return id;
        }
    
    //función para pedir el id del hotel
    public String mensajeIDHotel() {
        System.out.println("Introduce id de hotel: ");
        String id = fun.pedirString();
        return id;
        }
    //función para pedir el usuario
    public String mensajeUsuario() {
        System.out.println("Usuario: ");
        String usuario = fun.pedirString();
        return usuario;
        }
    //función para pedir el password
    public String mensajePass() {
        System.out.println("Password: ");
        String pass = fun.pedirString();
        return pass;
        }
    ////función para pedir el rol
    public int mensajeRol() throws IOException {
        System.out.println("Tu rol? (Admin = 1; turista = 2): ");
        int rol = fun.pedirInt();
        return rol;
        }
    
}
