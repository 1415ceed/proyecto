package vista;

import java.io.IOException;
import java.util.HashSet;
import java.util.Iterator;
import modelo.Turista;

/**
 * Fichero: VistaTurista.java
 * @author Camelia Gheorghiu <kami.cg78@gmail.com>
 * @date Nov 13, 2015
 */
public class VistaTurista implements IVista<Turista> {
   FuncionesTerminal terminal = new FuncionesTerminal(); 
   
       @Override
       public Turista obtener(){
           
        Turista t;
        VistaPrincipal vista = new VistaPrincipal();
        String nombre, apellido, dni, pass,usuario;
        int rol = 0;
        String idTurista;
        boolean opcion=true;
        
        System.out.println("\n INTRODUCIR DATOS");
        System.out.println("-------------------------------");
        System.out.print("Id: ");
        idTurista = terminal.pedirString();
        System.out.print("Nombre: ");
        nombre = terminal.pedirString();
        System.out.print("Apellido: ");
        apellido = terminal.pedirString();
        System.out.print("Dni (ejemplo: 12345678A): ");
        dni = terminal.validarDni();
        System.out.print("Usuario: ");
        pass = terminal.pedirString();
        System.out.print("Password: ");
        usuario = terminal.pedirString();
        //elegimos si el usuario es admin o turista
        do {
            try {
                System.out.println("Elige tu rol: 1. Administrador; 2. Turista; ");
                rol = terminal.pedirInt();
                if (rol==1 || rol==2)
                    opcion = false;
            }catch (Exception e) {
                vista.error(2);
            }
        } while (opcion);
     
        
        t = new Turista(idTurista,nombre,apellido,rol,dni,pass,usuario);
        
      
        return t;
    }

    /**
     *
     * @param t
     */
    @Override
        public void mostrar(Turista t) {
  
        System.out.println("MOSTRAR DATOS TURISTA:");
        System.out.println("========================");
        System.out.println(" Id turista :"+t.getId()+
                            "\n Nombre: "+t.getNombre() +"  Apellido: "+t.getApellido());
        System.out.println("Dni: "+t.getDni());
        System.out.println("Rol: "+t.getRole());
    }
        
        public void mostrarTuristas(HashSet h) {
  
        Iterator iterator = h.iterator();
        Turista turista;

            while (iterator.hasNext()) {
                System.out.println("\n************");
                turista = (Turista) iterator.next();
                mostrar(turista);
                System.out.println("************");
            }
        }
        
        public char menuCrud() throws IOException{
	
                char letra;	
                System.out.println("\n MENÚ CRUD");
                System.out.println("=====================");
                System.out.println("v".charAt(0)+ ". Volver");
                System.out.println("-------------------------");
		System.out.println("r".charAt(0)+ ". Read");
		System.out.println("u".charAt(0)+ ". Update");
		System.out.println("d".charAt(0)+ ". Delete");
                System.out.print("Opción:  ");
                letra = FuncionesTerminal.PedirCaracter();
    
		return letra;

	}
        
        public String actualizar() {
        System.out.println("\nEscribe el código del turista que quieres modificar: ");
        String id = terminal.pedirString();
        return id;
        }
    
        public String borrar() {
        System.out.println("\nEscribe el código del turista que quieres borrar: ");
        String id = terminal.pedirString();
        return id;
        }
        
}
