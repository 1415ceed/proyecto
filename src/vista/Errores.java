package vista;

/**
 * Fichero: Errores.java
 * @author Camelia Gheorghiu <kami.cg78@gmail.com>
 * @date Nov 21, 2015
 */
public class Errores {
    
        
    //clase para almacenar los errores del programa
    public String opcion(int o){
    String mensaje = "";
    
    switch(o){
        case 1:
            mensaje = "Opción incorrecta. Debe ser una de las siguientes letras:s, t, h, r.";
            break;
        case 2:
            mensaje = "No es un número.";
            break;
        case 3:
            mensaje = "Debes rellenar los campos del turista. Pulsa la tecla t";
            break;
        case 4:
            mensaje = "Debes rellenar los campos del hotel. Pulsa la tecla h.";
            break;
        case 5:
            mensaje = "Opción incorrecta. Debe ser una de las siguientes letras: v, c, r, u, d";
            break;
        case 6:
            mensaje = "¡No existe este código!";
            break;
        case 7:
            mensaje = "¡Hay que crear una reserva!";
            break;
        case 8:
            mensaje = "No hay hoteles en la base de datos.";
            break;
        case 9:
            mensaje = "Usuario incorrecto.";
            break;
        case 10:
            mensaje = "Password incorrecto.";
            break;
        case 11:
            mensaje = "No eres administrador. No puedes entrar.";
            break;
    
    }
    
        return mensaje;
    }

}

