package vista;

import java.util.HashSet;
import java.util.Iterator;
import modelo.*;

/**
 * Fichero: VistaHotel.java
 * @author Camelia Gheorghiu <kami.cg78@gmail.com>
 * @date Nov 13, 2015
 */
public class VistaHotel implements IVista<Hotel> {
    FuncionesTerminal terminal = new FuncionesTerminal();

    /**
     *
     * @return
     */
    @Override
    public Hotel obtener(){
        
        Hotel h;        

        String idHotel;
        String nombreHotel;
        String paisHotel = "";
        System.out.println("\n DATOS RESERVA HOTEL:");
        System.out.println("---------------------------");
        System.out.print("Id Hotel: ");
        idHotel = terminal.pedirString();
        System.out.print("Nombre del Hotel: ");
        nombreHotel = terminal.pedirString();
        System.out.print("País: ");
        paisHotel = terminal.pedirString();

        h = new Hotel(idHotel,nombreHotel,paisHotel);
        return h;
    }
    
    @Override
    public void mostrar(Hotel h){

        System.out.println("\n MOSTRAR HOTEL");
        System.out.println("---------------------------");
        System.out.println(" Id hotel: "+h.getIdHotel()+"\n Nombre del hotel: "+h.getNombreHotel()
                            +"\n Pais: "+h.getPaisHotel()
                            );
    }
    
    //RECORREMOS Y MOSTRAMOS LA LISTA DE HOTELES
    public void mostrarHoteles(HashSet h) {
  
        Iterator iterator = h.iterator();
        Hotel hotel;

            while (iterator.hasNext()) {
                System.out.println("\n************");
                hotel = (Hotel) iterator.next();
                mostrar(hotel);
             System.out.println("************");
            }
        }
        
        public String actualizar() {
        System.out.println("\nEscribe el código del hotel que quieres modificar: ");
        String id = terminal.pedirString();
        return id;
        }
    
        public String borrar() {
        System.out.println("\nEscribe el código del hotel que quieres borrar: ");
        String id = terminal.pedirString();
        return id;
        }
}

