
package vista;

import modelo.Administrador;


/**
 *
 * @author camelia
 */
public class VistaAdministrador implements IVista<Administrador> {
    FuncionesTerminal terminal = new FuncionesTerminal(); 
    
    public Administrador obtener(){
        Administrador admin = new Administrador();
        String departamento;
        
        System.out.println("Si eres administrador indica el departamento: ");
        departamento = terminal.pedirString();
        
        admin.setDepartamento(departamento);
        System.out.println("\nSus datos personales han sido registrados.");
        return admin;
    }

    @Override
    public void mostrar(Administrador administrador) {
        
        System.out.println("Departamento: "+administrador.getDepartamento());
        
    }
}
