package vista;

import java.io.IOException;
import java.util.HashSet;
import java.util.Iterator;
import java.util.logging.Level;
import java.util.logging.Logger;
import modelo.*;

/**
 * Fichero: VistaReserva.java
 * @author Camelia Gheorghiu <kami.cg78@gmail.com>
 * @date Nov 13, 2015
 */
public class VistaReserva implements IVista<Reserva> {
    FuncionesTerminal terminal = new FuncionesTerminal();
    private IModelo modelo;

    /**
     *
     * @return
     */
    @Override
    public Reserva obtener() {

        Reserva r = new Reserva();
        
        VistaPrincipal vista = new VistaPrincipal();
        String idReserva;
        String fechaReserva = "";
        double precio = 0.0;
        boolean numeroInt = false; //var para comprobar si el precio es numérico
        
        System.out.println("\nINTRODUCIR DATOS RESERVA");
        System.out.println("------------------------------");
        System.out.println("Introduce el código la reserva: ");
        idReserva = terminal.pedirString();
        System.out.print("Fecha reserva: ");
        fechaReserva = terminal.pedirString();

        //comprobamos si precio es número
        while (!numeroInt) {
            System.out.print("Precio: ");
            try {
                precio = terminal.pedirInt();
                numeroInt = true;
            } catch (NumberFormatException e) {
                //Si el precio no es número mensaje de error
                vista.error(2);

            } catch (IOException ex) {
                Logger.getLogger(VistaReserva.class.getName()).log(Level.SEVERE, null, ex);
            }
        }

        System.out.println();
        
        //r =new Reserva(idReserva, fechaReserva, precio);
        r.setIdReserva(idReserva);
        r.setFecha(fechaReserva);
        r.setPrecio(precio);

        return r;
    }

    /**
     *
     * @param r
     */
    @Override
    public void mostrar(Reserva r) {

       VistaTurista vt = new VistaTurista();
        VistaHotel vh = new VistaHotel();
        
        System.out.println("----------------------");
        System.out.println("\nMOSTRAR RESERVA");
        System.out.println("============================");
        System.out.println("* Código reserva: " + r.getIdReserva());
        System.out.println("-------------------------");
        vt.mostrar(r.getTurista());
        System.out.println("*");
        vh.mostrar(r.getHotel());
        System.out.println("-------------------------");
        System.out.println(" Fecha reserva: " + r.getFecha() + "\n Precio: " + r.getPrecio());
        System.out.println();
        System.out.println("RESERVA CREADA CON ÉXITO!!");
        System.out.println("----------------------");
    }
    
    //RECORREMOS LA LISTA DE HOTELES Y LA MOSTRAMOS
    public void mostrarReservas(HashSet h) {
  
        Iterator iterator = h.iterator();
        Reserva reserva;

            while (iterator.hasNext()) {
                reserva = (Reserva) iterator.next();
                mostrar(reserva);
            }
        }
        
        public String actualizar() {
        System.out.println("\nEscribe el código de la reserva que quieres modificar: ");
        String id = terminal.pedirString();
        return id;
        }
    
        public String borrar() {
        System.out.println("\nEscribe el código de la reserva que quieres borrar: ");
        String id = terminal.pedirString();
        return id;
        }      
}

