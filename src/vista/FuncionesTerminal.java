package vista;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Fichero: FuncionesTerminal.java
 * @author Camelia Gheorghiu <kami.cg78@gmail.com>
 * @date Nov 13, 2015
 */
public class FuncionesTerminal {
   
    public String pedirString(){
        
        InputStreamReader input = new InputStreamReader(System.in);
        BufferedReader buffer = new BufferedReader(input);
        String linea = null;
        try{
            linea= buffer.readLine();
            
        }catch (Exception e){
            
        }
        
        return linea;
    }
    
    
    public int pedirInt() throws IOException{
        
        InputStreamReader input = new InputStreamReader(System.in);
        BufferedReader buffer = new BufferedReader(input);
        String linea;
        int num = 0;
        
            linea= buffer.readLine();
            num = Integer.parseInt(linea);

        return num;
    }
    
    public static char PedirCaracter() throws IOException {

    InputStreamReader isr = new InputStreamReader(System.in);
    char c = ' ';
    c = (char) isr.read();
    return c;
  }
    
    //función para pedir el dni validándolo con una expresión regular
    public String validarDni() {
        String dni;
       // System.out.println("Introduce dni: ");
        dni = pedirString();
        Pattern patron = Pattern.compile ("(\\d{8})([-]?)([A-Z]{1})");
        
        Matcher encaja = patron.matcher(dni);
        if (!encaja.find()) {
            System.out.println("El DNI introducido no es correcto");
            return validarDni();
        }else {
            
            return dni;
        }
    
     }
    
    
      
    
    public void salir(){
        
        System.out.println("\nGRACIAS POR SU ATENCIÓN");
    }
    
    public void volver(){
        
        System.out.println("\nVolviendo...");
    }
    
    public static void mostrarTexto(String texto) {
        System.out.println(texto);
    }
    
    public static void mostrarError(String error) {
        System.err.println(error);
    }

}

