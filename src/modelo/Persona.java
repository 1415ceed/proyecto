package modelo;

/**
 *Fichero: Persona.java
 * @author Camelia Gheorghiu <camelianaa@hotmail.com>
 * @date 15-dic-2015
 */
public class Persona {
    private String idPersona;
    private String nombre;
    private String apellido;
    private int role;
    private String usuario;
    private String password;
    
      public Persona () {
        idPersona = "";
        nombre = "";
        apellido = "";
        role = 0;
        usuario = "";
        password = "";
    }
  
  public Persona (String idP, String n, String a, int r, String u, String p) {
    idPersona = idP;
    nombre = n;
    apellido = a;
    role = r;
    usuario = u;
    password = p;
  }
    /**
     * @return the id
     */
    public String getId() {
        return idPersona;
    }

    /**
     * @param id the id to set
     */
    public void setId(String id) {
        this.idPersona = id;
    }

    /**
     * @return the nombre
     */
    public String getNombre() {
        return nombre;
    }

    /**
     * @param nombre the nombre to set
     */
    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    /**
     * @return the apellido
     */
    public String getApellido() {
        return apellido;
    }

    /**
     * @param apellido the apellido to set
     */
    public void setApellido(String apellido) {
        this.apellido = apellido;
    }

    /**
     * @return the usuario
     */
    public String getUsuario() {
        return usuario;
    }

    /**
     * @param usuario the usuario to set
     */
    public void setUsuario(String usuario) {
        this.usuario = usuario;
    }

    /**
     * @return the password
     */
    public String getPassword() {
        return password;
    }

    /**
     * @param password the password to set
     */
    public void setPassword(String password) {
        this.password = password;
    }

    /**
     * @return the role
     */
    public int getRole() {
        return role;
    }

    /**
     * @param role the role to set
     */
    public void setRole(int role) {
        this.role = role;
    }
}
