package modelo;

import java.util.HashSet;

/**
 *Fichero: ModeloArray.java
 * @author Camelia Gheorghiu <camelianaa@hotmail.com>
 * @date 08-dic-2015
 */
public class ModeloArray implements IModelo{
    
    Turista turistas[] = new Turista[100];
    Turista vaciot = new Turista("", "", "",0,"","","");
    
    Hotel hoteles[] = new Hotel[100];
    Hotel vacioh = new Hotel("", "", "");
    
    Reserva reservas[] = new Reserva[100];
    Reserva vacior = new Reserva("", "", 0.0, null, null);
    
    Administrador administradores[] = new Administrador[100];
    Administrador vacioadmin = new Administrador("", "");
    
    int idt = 0;
    int idh = 0;
    int idr = 0;
    int idadmin = 0;
       
    public ModeloArray() {
        //Inicializamos todo el vector a vacíos
        for (int i = 0; i < turistas.length; i++) {
            turistas[i] = vaciot;
        }
        for (int i = 0; i < hoteles.length; i++) {
            hoteles[i] = vacioh;
        }
        for (int i = 0; i < reservas.length; i++) {
            reservas[i] = vacior;
        } 
        for (int i = 0; i < administradores.length; i++) {
            administradores[i] = vacioadmin;
        }
    }

    @Override
    public void create(Turista t) {
        turistas[idt] = t;
        idt++; //incrmentamos en 1 cada vez que se crea
    }
    
    @Override
    public void create(Hotel h) {
        hoteles[idh] = h;
        idh++;
    }
    
    @Override
    public void create(Reserva r) {
        reservas[idr] = r;
        idr++;
    }
    
      @Override
    public void create(Administrador admin) {
        administradores[idadmin] = admin;
        idadmin++;
    }

   @Override
    public HashSet<Turista> readTurista() {
      HashSet hs = new HashSet();
      Turista t = null;
      int i=0;
      //recoremos la lista y la devolvemos
      while (i < idt ) {
          t = turistas[i];
          hs.add(t);
          i++;
      }
      return hs;
    }
    
    @Override
    public HashSet<Hotel> readHotel() {
      HashSet hs = new HashSet();
      Hotel h = null;
      int i=0;
      while (i < idh ) {
          h = hoteles[i];
          hs.add(h);
          i++;
        }
      return hs;
    }
    
    @Override
    public HashSet<Reserva> readReserva() {
        HashSet hs = new HashSet();
        Reserva r = null;
        int i=0;
      while (i < idr ) {
        r = reservas[i];
          hs.add(reservas[i]);
          i++;
        }
      return hs;
    }
    
    @Override
    public void update(Turista t) {
        int i = 0;
        //recorremos todo el vector hasta que el id coincida, si coincide lo cambia
        while (i < turistas.length) {
            if (turistas[i].getId().equals(t.getId())) {
            turistas[i] = t;
            }
        i++;
        }
    }
    
    @Override
    public void update(Hotel h) {
        int i = 0;
        while (i < hoteles.length) {
            if (hoteles[i].getIdHotel().equals(h.getIdHotel())) {
            hoteles[i] = h;
            }
        i++;
        }
    }
    
    @Override
    public void update(Reserva r) {
        int i = 0;
        while (i < reservas.length) {
            if (reservas[i].getIdReserva().equals(r.getIdReserva())) {
            reservas[i] = r;
            }
        i++;
        }
    }

    @Override
    public void delete(Turista t) {
        int i = 0;
        //recorremos todo el vector hasta que el id coincida, si coincide lo borra
        while (i < turistas.length) {
            if (turistas[i].getId().equals(t.getId())) {
                turistas[i] = vaciot;
            }
            i++;
        }
    }

    @Override
    public void delete(Hotel h) {
        int i = 0;
        while (i < hoteles.length) {
            if (hoteles[i].getIdHotel().equals(h.getIdHotel())) {
                hoteles[i] = vacioh;
            }
            i++;
        }
    }

    @Override
    public void delete(Reserva r) {
        int i = 0;
        while (i < reservas.length) {
            if (reservas[i].getIdReserva().equals(r.getIdReserva())) {
                reservas[i] = vacior;
            }
            i++;
        }
    }

}