package modelo;

/**
 * Fichero: Turista.java
 * @author Camelia Gheorghiu <kami.cg78@gmail.com>
 * @date Nov 13, 2015
 */
public class Turista extends Persona {
    
    private String dni;
    
    /**
     *Constructor
     */
    public Turista(){
        
        dni = "";
    }
    public Turista(String idP, String n, String a, int r, String d, String u, String p){
        super(idP, n, a, r, u, p);
        dni = d;
    }

    /**
     * @return the dni
     */
    public String getDni() {
        return dni;
    }

    /**
     * @param dni the dni to set
     */
    public void setDni(String dni) {
        this.dni = dni;
    }
}
