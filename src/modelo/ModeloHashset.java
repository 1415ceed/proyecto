package modelo;

import java.util.HashSet;
import java.util.Iterator;

/**
 * Fichero: ModeloHashset.java
 * @author Camelia Gheorghiu <camelianaa@hotmail.com>
 * @date 03-dic-2015
 */
public class ModeloHashset implements IModelo {
    
    private HashSet<Turista> turistas = new HashSet();
    private HashSet<Hotel> hoteles = new HashSet();
    private HashSet<Reserva> reservas = new HashSet();
    private HashSet<Administrador> administradores = new HashSet();
    
    int idt = 0;
    int idh = 0;
    int idr = 0;
    int idadmin = 0;
    
      //agregamos elementos con el método add que recibe por parámetro objetos de tipo Turista, Hotel y Reserva
    @Override
    public void create(Turista t) {
        turistas.add(t);
        idt++;
    }
    
    @Override
    public void create(Hotel h) {
        hoteles.add(h);
        idh++;
    }
    
    @Override
    public void create(Reserva r) {
        reservas.add(r);
        idr++;
    }
    
     @Override
    public void create(Administrador admin) {
        administradores.add(admin);
        idadmin++;
    }
    
    //devolvemos las listas de los elementos creados
    @Override
    public HashSet<Turista> readTurista() {
        return turistas;  
    }
    
     @Override
    public HashSet<Hotel> readHotel() {
        return hoteles;
    }
    
    @Override
    public HashSet<Reserva> readReserva() {
        return reservas;
    }
    
    //actualizamos un objeto tomando como referencia el id
    @Override
    public void update(Turista t) {
        //Obtenemos un iterator y recorremos la lista
        Iterator iter = turistas.iterator();
        Turista tur;
            /*comprobamos si hay elementos en la lista y los recorre hasta que el id coincide con el que buscamos
        y si coincide cambia los datos del objeto*/
            while(iter.hasNext()){
                tur = (Turista) iter.next();
                    if(tur.getId().equals(t.getId())) {
			iter.remove();//borra objeto
                    }
                turistas.add(tur);//añade objeto al final de la lista
            }
    }
    
    //actualizamos un objeto tomando como referencia el id
    @Override
    public void update(Hotel h) {
        //Obtenemos un iterator y recorremos la lista
        Iterator iter = hoteles.iterator();
        Hotel hot;
            /*comprobamos si hay elementos en la lista y los recorre hasta que el id coincide con el que buscamos
        y si coincide cambia los datos del objeto*/
            while(iter.hasNext()){
                hot = (Hotel) iter.next();
		if(hot.getIdHotel().equals(h.getIdHotel())) {
				iter.remove();//borra objeto
			}
                        hoteles.add(hot);//añade objeto
		}
    }
    
    //actualizamos un objeto tomando como referencia el id
    @Override
    public void update(Reserva r) {
        //Obtenemos un iterator y recorremos la lista
        Iterator iter = reservas.iterator();
        Reserva res;
            /*comprobamos si hay elementos en la lista y los recorre hasta que el id coincide con el que buscamos
        y si coincide cambia los datos del objeto*/
		while(iter.hasNext()){
                    res = (Reserva) iter.next();
			if(res.getIdReserva().equals(r.getIdReserva())) {
                            iter.remove(); //borra objeto
			}
                        reservas.add(res); //añade objeto
		}
    }
    
    //de la misma forma, para borrar recorremos la lista tomando como referencia el id
    @Override
    public void delete(Turista t) {
        //Obtenemos un iterator y recorremos la lista
        Iterator iter = turistas.iterator();
        Turista tur;
        /*comprobamos si hay elementos en la lista y los recorre hasta que el id coincide con el que buscamos
        y si coincide cambia los datos del objeto*/
		while(iter.hasNext()){
                    tur = (Turista) iter.next();
			if(tur.getId().equals(t.getId())) {
				iter.remove(); //elimina objeto
			}
		}
    }

    @Override
    public void delete(Hotel h) {
        //Obtenemos un iterator y recorremos la lista
        Iterator iter = hoteles.iterator();
        Hotel hot;
            /*comprobamos si hay elementos en la lista y los recorre hasta que el id coincide con el que buscamos
        y si coincide cambia los datos del objeto*/
		while(iter.hasNext()){
                    hot = (Hotel) iter.next();
			if(hot.getIdHotel().equals(h.getIdHotel())) {
				//elimina objeto
				iter.remove();
			}
		}
    }


    @Override
    public void delete(Reserva r) {
        //Obtenemos un iterator y recorremos la lista
        Iterator iter = reservas.iterator();
        Reserva res;
            /*comprobamos si hay elementos en la lista y los recorre hasta que el id coincide con el que buscamos
        y si coincide cambia los datos del objeto*/
		while(iter.hasNext()){
                    res = (Reserva) iter.next();
			if(res.getIdReserva().equals(r.getIdReserva())) {
				//elimina objeto
				iter.remove();
			}
		}
    }
}