package modelo;

/**
 * Fichero: Reserva.java
 * @author Camelia Gheorghiu <kami.cg78@gmail.com>
 * @date Nov 13, 2015
 */
public class Reserva {

    private String id;
    private String fecha;
    private double precio;
    private Turista turista;
    private Hotel hotel;
    
    public Reserva(){
        id = "";
        fecha = "";
        precio = 0.0;
        turista = null;
        hotel = null;
    
    }
    
    public Reserva(String i, String f, double p){
        id = i;
        fecha = f;
        precio = p;
    }
    
    public Reserva(String i, String f, double p, Turista t, Hotel h){
        id = i;
        fecha = f;
        precio = p;
        turista = t;
        hotel = h;
    
    }

    /**
     * @return the codr
     */
    public String getIdReserva() {
        return id;
    }

    /**
     * @param codr the codr to set
     */
    public void setIdReserva(String codr) {
        id = codr;
    }

    /**
     * @return the fecha
     */
    public String getFecha() {
        return fecha;
    }

    /**
     * @param fecha the fecha to set
     */
    public void setFecha(String fecha) {
        this.fecha = fecha;
    }

    /**
     * @return the precio
     */
    public double getPrecio() {
        return precio;
    }

    /**
     * @param precio the precio to set
     */
    public void setPrecio(double precio) {
        this.precio = precio;
    }

    /**
     * @return the turista
     */
    public Turista getTurista() {
        return turista;
    }

    /**
     * @param turista the turista to set
     */
    public void setTurista(Turista turista) {
        this.turista = turista;
    }

    /**
     * @return the hotel
     */
    public Hotel getHotel() {
        return hotel;
    }

    /**
     * @param hotel the hotel to set
     */
    public void setHotel(Hotel hotel) {
        this.hotel = hotel;
    }

}

