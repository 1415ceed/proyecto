package modelo;

/**
 * Fichero: Hotel2.java
 * @author Camelia Gheorghiu <kami.cg78@gmail.com>
 * @date Nov 13, 2015
 */
public class Hotel {
    
    private String id;
    private String nombre;
    private String pais;
    
    /**
     *Constructor
     */
    public Hotel(){
        id = "";
        nombre = "";
        pais= "";
    }
    
    /**
     *Constructor
     * @param codh
     * @param nombre
     * @param pais
     */
    public Hotel (String codh, String nombre, String pais){
        this.id = codh;
        this.nombre = nombre;
        this.pais = pais;
    }
    
    /** 
     * @param codigo the CodigoHotel to set
     */
    public void setIdHotel(String codigo){
        id = codigo;
    }
    
    /**
     *
     * @return codh
     */
    public String getIdHotel(){
        return id;
    }
    
    /**
     *
     * @param nombre the NombreHotel to set
     */
    public void setNombreHotel(String nombre){
        this.nombre = nombre;
    }
    
    /**
     *
     * @return nombre
     */
    public String getNombreHotel(){
        return nombre;
    }

    /**
     *
     * @param pais the PaisHotel to set
     */
    public void setPaisHotel(String pais){
        this.pais = pais;
    }

    /**
     *
     * @return pais
     */
    public String getPaisHotel(){
        return pais;
    }

}
