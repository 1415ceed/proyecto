package modelo;

/**
 *
 * @author camelia
 */
    public class Administrador extends Persona {
        private String idAdmin;
        private String departamento;
        
        public Administrador (){
        
        departamento = "";
    }
        public Administrador (String id, String dep){
        
        idAdmin = id;
        departamento = dep;
    }
    
    /**
     * @return the departamento
     */
    public String getDepartamento() {
        return departamento;
    }

    /**
     * @param departamento the departamento to set
     */
    public void setDepartamento(String departamento) {
        this.departamento = departamento;
    }
    
}