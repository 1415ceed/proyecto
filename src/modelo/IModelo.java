package modelo;

import java.util.HashSet;

/**
 * Fichero: IModelo.java
 * @author Camelia Gheorghiu <camelianaa@hotmail.com>
 * @date 03-dic-2015
 */
public interface IModelo {
    
    //TURISTA
    public void create(Turista t); //Crea uno nuevo
    public HashSet<Turista> readTurista(); //Obtiene todos
    public void update(Turista t); //Actualiza el indicado
    public void delete(Turista t); //Borra el objeto indicado
    
    //HOTEL
    public void create(Hotel h); //Crea uno nuevo
    public HashSet <Hotel> readHotel(); //Obtiene todos
    public void update(Hotel h); //Actualiza el indicado
    public void delete(Hotel h); //Borra el objeto indicado
    
    //RESERVA
    public void create(Reserva r); //Crea uno nuevo
    public HashSet <Reserva> readReserva(); //Obtiene todos
    public void update(Reserva r); //Actualiza el indicado
    public void delete(Reserva r); //Borra el objeto indicado

    public void create(Administrador a); //Crea uno nuevo
}

